This resource pack retextures all birch leaves to make them pink.

![A forest containing some pink-leaved birch trees.](./.images/trees.png)
